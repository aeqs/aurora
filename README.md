# AURORA

## AEQs
This project is part of the AEQs project collection by Arved Becker and Kilian Chung.

## Description
Aurora is a project that focuses on training AI to excel at the 2048 game.

It uses N-Tuple-Networks for decision making and a temporal difference learning approach.
The player agents evaluate the afterstates for each possible move and are trained with a variant of TD(0) that takes the possible afterstates of the next move into account.

## Quick Start
An ant build.xml file is provided.
To execute the program, use target `run`.

Run example:
```
git clone https://gitlab.com/aeqs/aurora.git
cd aurora
ant run
```

For programm help page, execute:
```
ant compile
java -cp build main/Main
```

## Configuration
Refer to [Configuration](cfg/README.md).

## Training [![pipeline status](https://gitlab.com/aeqs/aurora/badges/main/pipeline.svg)](https://gitlab.com/aeqs/aurora/-/commits/main)
Check out the latest training [stats](https://aeqs.gitlab.io/aurora).

## Wiki
For more information and details, please check out the [wiki](https://gitlab.com/aeqs/aurora/-/wikis/home).
