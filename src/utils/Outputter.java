package utils;

/**
 * Wrapper class for System out
 **/
public class Outputter {

    /**
     * Color codes conforming to ANSI escape sequences
     **/
    public static final String NORMAL = "\u001b[0m", GRAY = "\u001b[1;30m", RED = "\u001b[1;31m", GREEN = "\u001b[32m", BLUE = "\u001b[1;34m", MAGENTA = "\u001b[1;35m";

    /**
     * Clear line until end conforming to ANSI escape sequences
     **/
    public static final String CLEAR_LINE = "\u001b[0K";

    /**
     * Prints object with newline.
     *
     * @param object object to be printed
     **/
    public static void println(Object object) {
        println(object.toString());
    }

    /**
     * Prints message with newline.
     *
     * @param message string to be printed
     **/
    public static void println(String message) {
        System.out.println(message);
    }

    /**
     * Uses ANSI escape code to signal going up n lines.
     *
     * @param n the number of lines to go up
     **/
    public static void goNLinesUp(int n) {
        System.out.println("\u001b[" + n + "A");
    }
}
