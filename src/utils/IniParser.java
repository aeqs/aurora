package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

/**
 * A parser class to parse INI files
 *
 * Syntax rules are as followed:
 *   - A semicolon denotes a comment until a linebreak
 *   - Comments and empty lines are ignored by the parser
 *   - If a line contains an equal sign, the string to the left of the first occurence will be interpreted as key, while the right side will be interpreted as value
 *   - Lines containing a string surrounded by square brackets indicate the beginning of a new section named after the string inside the brackets
 *   - Every key, value and section will be trimmed, i.e. whitespaces before and after will be stripped
 *   - Keys and sections are case-insensitive (converted to lowercase) and must satisfy following regex: [a-zA-Z0-9\.-_]+
 *   - Values may contain any character
 **/
public class IniParser {

    /**
     * Parses an INI file into a map.
     *
     * This map contains seperate maps for each section.
     *
     * All keys and sections are lowercase.
     * To get a value, first get the section map and then get the value with the key.
     *
     * @param iniFile the INI file to be parsed
     * @return a map containing a map for each section containing string key-value pairs each
     * @throws IOException on error opening or reading file
     * @throws ParseException on syntax error
     **/
    public static Map<String, Map<String, String>> parse(File iniFile) throws IOException, ParseException {
        Map<String, Map<String, String>> map = new HashMap<>();
        BufferedReader reader = new BufferedReader(new FileReader(iniFile));
        String line, key, value, section = null;
        int lineCounter = 0, byteCounter = 0;

        // map for values without section
        map.put(null, new HashMap<>());

        while ((line = reader.readLine()) != null) {
            byteCounter += line.length() + 1;
            ++lineCounter;

            // handle comments
            if (line.contains(";"))
                line = line.substring(0, line.indexOf(';'));

            // handle blank lines
            if (line.isBlank())
                continue;

            // handle assignments
            if (line.contains("=")) {
                key = line.substring(0, line.indexOf('=')).trim().toLowerCase();
                value = line.substring(line.indexOf('=') + 1).trim();

                // handle invalid key name
                if (!key.matches("[a-z0-9\\.-_]+")) {
                    reader.close();

                    throw new ParseException("Invalid key name in line " + lineCounter, byteCounter - 1);
                }

                map.get(section).put(key, value);
            } else if (line.matches("\\s*\\[.*\\]\\s*")) {
                // remove null-section if empty
                if (section == null && map.get(null).isEmpty())
                    map.remove(null);

                // handle sections
                section = line.substring(line.indexOf('[') + 1, line.lastIndexOf(']')).trim().toLowerCase();

                // handle invalid section name
                if (!section.matches("[a-z0-9\\.-_]+")) {
                    reader.close();

                    throw new ParseException("Invalid section name in line " + lineCounter, byteCounter - 1);
                }

                // new map if section does not already have one
                if (!map.containsKey(section))
                    map.put(section, new HashMap<>());
            } else {
                // handle syntax error
                reader.close();

                throw new ParseException("Syntax error in line " + lineCounter, byteCounter - 1);
            }
        }

        reader.close();

        return map;
    }
}
