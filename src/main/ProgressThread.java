package main;

import static utils.Outputter.*;

import java.util.Arrays;

/**
 * A class to print the progress of a number of worker threads
 **/
public class ProgressThread extends Thread {

    /**
     * Width of progress bar
     **/
    private static final int WIDTH = 50;

    /**
     * Interval to print the progress in milliseconds
     **/
    private static final int INTERVAL_MS = 1000;

    /**
     * Observed runner threads
     **/
    private final WorkerThread[] workers;

    /**
     * Creates a new progress thread.
     *
     * @param workers the worker threads to be observed
     **/
    public ProgressThread(WorkerThread... workers) {
        // name thread
        super("Progress Thread");

        this.workers = workers;
    }

    /**
     * Periodically, prints progress of all worker threads.
     **/
    @Override
    public void run() {
        long start = System.currentTimeMillis();
        int maxNameLength = 0;
        boolean allDead;

        for (WorkerThread worker : workers) {
            if (worker.runnerName.length() > maxNameLength)
                maxNameLength = worker.runnerName.length();

            // print empty lines before first iteration of progress loop to compensate going lines up
            println("");
        }
        println("\n");

        do {
            goNLinesUp(workers.length + 2);

            // header
            println(NORMAL + "Progress of agents (" + ((System.currentTimeMillis() - start) / 1000) + "s):");
            allDead = true;

            // sort threads by progress, descending
            Arrays.sort(workers, (a, b) -> (a.getProgress() > b.getProgress() ? -1 : 1));

            for (WorkerThread worker : workers) {
                int progress;
                WorkerThread.State currentState = worker.getCurrentState();
                // status color, name
                String line = CLEAR_LINE + (
                        currentState.equals(WorkerThread.State.READY) ? NORMAL :
                        currentState.equals(WorkerThread.State.RUNNING) ? BLUE :
                        currentState.equals(WorkerThread.State.FINISHED) ? GREEN : RED) +
                    "  | " + MAGENTA + worker.runnerName + NORMAL;

                // fill shorter names with spaces
                for (int i = 0; i < maxNameLength - worker.runnerName.length(); ++i)
                    line += ' ';

                // print progress bar
                line += " : [" + BLUE;
                for (progress = 0; progress < Math.floor(worker.getProgress() * WIDTH); ++progress)
                    line += '#';
                if (progress++ < WIDTH)
                    line += MAGENTA + '>' + RED;
                for (; progress < WIDTH; ++progress)
                    line += '-';
                line += NORMAL + ']';

                // message
                switch (currentState) {
                    case READY:
                        line += NORMAL + "   Not yet started...";
                        break;
                    case RUNNING:
                        double rounded = Math.round(worker.getProgress() * 10000) / 100.0;
                        line += BLUE + " - " + NORMAL + rounded + "%";
                        allDead = false;
                        break;
                    case FINISHED:
                        line += GREEN + " +" + NORMAL + " Finished execution in " + worker.getExecutionTime() + "ms.";
                        break;
                    case FAILED:
                        line += RED + " x" + NORMAL + " Errored out!";
                        break;
                }

                println(line);
            }

            try {
                Thread.sleep(INTERVAL_MS);
            } catch (InterruptedException e) {
                System.err.println("Progress thread interrupted: " + e.getMessage());
                // continue, since progress thread is not essential
            }
        } while (!allDead);
        // run until all worker threads have finished execution
    }
}
