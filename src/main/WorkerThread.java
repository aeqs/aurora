package main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Writer;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.BiFunction;

import _2048.Game;
import ntn.NTupleNetwork;
import player.Agent;

/**
 * A thread class associated with one runner
 **/
public class WorkerThread extends Thread {

    /**
     * All possible states for worker threads
     **/
    public enum State {
        READY,
        RUNNING,
        FINISHED,
        FAILED,
    }

    /**
     * Builder class, containing default values
     **/
    public static class Builder {

        /**
         * Name of the runner
         **/
        private String runnerName;

        /**
         * Agent instance produced by this builder
         **/
        private Agent agent = null;

        /**
         * Binary agent source file
         **/
        private File agentSource = null;

        /**
         * Learning rate for network
         **/
        private double learningRate = 0.01;

        /**
         * Indicator if learning rate was explicitly set
         *
         * Iff learning rate is set with this builder, it will yield true.
         * If true, builders learning rate will be forced on deserialized agents.
         **/
        private boolean explicitLearningRate = false;

        /**
         * Network string for the agent
         **/
        private String network = "0-1-4-5-8-9,1-2-5-6-9-10,2-6-10-14,3-7-11-15";

        /**
         * Flag for taking symmetries of board positions into account
         *
         * If true, agent will additionally consider rotations and reflections of every board position.
         **/
        private boolean symmetricSampling = true;


        /**
         * Number of iterations
         **/
        private int iterations = 10000;

        /**
         * Number of redundance runs per iteration
         **/
        private int redundanceRuns = 5;

        /**
         * Binary agent dump file
         **/
        private File agentDump = null;

        /**
         * Score dump file
         **/
        private File scoreDump = null;

        /**
         * Win rate dump file
         **/
        private File winRateDump = null;

        /**
         * Number of iterations between agent dumps
         **/
        private int agentDumpInterval = 2000;

        /**
         * Creates a new builder.
         *
         * @return empty builder instance
         **/
        public static Builder create() {
            return new Builder();
        }

        /**
         * Set runner name.
         *
         * @param runnerName new name for this runner
         * @return this builder
         **/
        public Builder setRunnerName(String runnerName) {
            this.runnerName = runnerName;
            return this;
        }

        /**
         * Sets agent source file.
         *
         * @param agentSource binary agent source file
         * @return this builder
         **/
        public Builder setAgentSource(File agentSource) {
            this.agentSource = agentSource;
            return this;
        }

        /**
         * Sets learning rate.
         *
         * If called, agent learning rate will be overwritten.
         *
         * @param learningRate new learning rate value
         * @return this builder
         **/
        public Builder setLearningRate(double learningRate) {
            this.learningRate = learningRate;
            explicitLearningRate = true;
            return this;
        }

        /**
         * Sets network for agent.
         *
         * @param network string representation for the agent's network
         * @return this builder
         **/
        public Builder setNetwork(String network) {
            this.network = network;
            return this;
        }

        /**
         * Sets symmetric sampling.
         *
         * @param symmetricSampling new value for symmetric sampling flag
         * @return this builder
         **/
        public Builder setSymmetricSampling(boolean symmetricSampling) {
            this.symmetricSampling = symmetricSampling;
            return this;
        }

        /**
         * Sets number of iterations.
         *
         * @param iterations new number of iterations
         * @return this builder
         **/
        public Builder setIterations(int iterations) {
            this.iterations = iterations;
            return this;
        }

        /**
         * Sets number of redundance runs.
         *
         * @param redundanceRuns new number of redundance runs
         * @return this builder
         **/
        public Builder setRedundanceRuns(int redundanceRuns) {
            this.redundanceRuns = redundanceRuns;
            return this;
        }

        /**
         * Sets agent dump file.
         *
         * @param agentDump file for agent dumps
         * @return this builder
         **/
        public Builder setAgentDump(File agentDump) {
            this.agentDump = agentDump;
            return this;
        }

        /**
         * Sets score dump file.
         *
         * @param scoreDump file for score dumps
         * @return this builder
         **/
        public Builder setScoreDump(File scoreDump) {
            this.scoreDump = scoreDump;
            return this;
        }

        /**
         * Sets win rate dump file.
         *
         * @param winRateDump file for win rate dumps
         * @return this builder
         **/
        public Builder setWinRateDump(File winRateDump) {
            this.winRateDump = winRateDump;
            return this;
        }

        /**
         * Set agent dump interval.
         *
         * @param agentDumpInterval number of iterations between agent dumps
         * @return this builder
         **/
        public Builder setAgentDumpInterval(int agentDumpInterval) {
            this.agentDumpInterval = agentDumpInterval;
            return this;
        }

        /**
         * Builds the worker thread instance.
         *
         * Settings previously specified will be used.
         *
         * @return a thread instance with the specified settings
         **/
        public WorkerThread build() {
            if (agentSource != null) {
                agent = Agent.deserializeFromFile(agentSource);

                if (agent == null)
                    System.err.println("Agent could not be deserialized, building new one...");
                else if (explicitLearningRate)
                    agent.setLearningRate(learningRate);
            }

            if (agent == null)
                agent = new Agent(new NTupleNetwork(symmetricSampling, network), learningRate);

            return new WorkerThread(runnerName, agent, iterations, redundanceRuns, agentDump, scoreDump, winRateDump, agentDumpInterval);
        }
    }

    /**
     * Map containing all possible configuration keys
     *
     * Every key is linked to an action to be performed on a worker thread builder.
     * This helps with building the actual thread instances.
     **/
    private static final Map<String, BiFunction<WorkerThread.Builder, String, WorkerThread.Builder>> builderActions = new HashMap<>();

    /**
     * Fill the action map
     **/
    static {
        builderActions.put("agent_source",        (builder, value) -> builder.setAgentSource(new File(value)));
        builderActions.put("learning_rate",       (builder, value) -> builder.setLearningRate(Double.valueOf(value)));
        builderActions.put("network",             (builder, value) -> builder.setNetwork(value));
        builderActions.put("symmetric_sampling",  (builder, value) -> builder.setSymmetricSampling(Boolean.valueOf(value)));
        builderActions.put("iterations",          (builder, value) -> builder.setIterations(Integer.valueOf(value)));
        builderActions.put("redundance_runs",     (builder, value) -> builder.setRedundanceRuns(Integer.valueOf(value)));
        builderActions.put("agent_dump",          (builder, value) -> builder.setAgentDump(new File(value)));
        builderActions.put("score_dump",          (builder, value) -> builder.setScoreDump(new File(value)));
        builderActions.put("win_rate_dump",       (builder, value) -> builder.setWinRateDump(new File(value)));
        builderActions.put("agent_dump_interval", (builder, value) -> builder.setAgentDumpInterval(Integer.valueOf(value)));
    }

    /**
     * Parse INI file that specifies a configuration for agents.
     *
     * @param iniFile the file containing agent configuration
     * @return executable jobs for each agent configuration
     **/
    public static WorkerThread[] workerThreadsFromIniFile(File iniFile) {
        WorkerThread[] threads;

        try {
            Map<String, Map<String, String>> runnerConfigs = utils.IniParser.parse(iniFile);

            // remove null-runner
            runnerConfigs.remove(null);

            threads = new WorkerThread[runnerConfigs.keySet().size()];
            int threadIndex = 0;

            for (Map.Entry<String, Map<String, String>> runnerConfig : runnerConfigs.entrySet()) {
                String runID = runnerConfig.getKey();
                Map<String, String> runConfig = runnerConfig.getValue();
                WorkerThread.Builder builder = WorkerThread.Builder.create().setRunnerName(runID);

                for (Map.Entry<String, String> runConfigEntry : runConfig.entrySet()) {
                    String setting = runConfigEntry.getKey();
                    String value = runConfigEntry.getValue();
                    BiFunction<WorkerThread.Builder, String, WorkerThread.Builder> action = builderActions.get(setting);

                    if (action != null)
                        action.apply(builder, value);
                }

                threads[threadIndex++] = builder.build();
            }
        } catch(ParseException | IOException e) {
            threads = new WorkerThread[0];

            System.err.println("Error reading file " + iniFile.getName() + ": " + e.getMessage());
        }

        return threads;
    }

    /**
     * Name of this runner
     **/
    public final String runnerName;

    /**
     * Agent instance for this worker thread
     **/
    private final Agent agent;

    /**
     * Number of iterations
     **/
    private final int iterations;

    /**
     * Number of redundance runs per iteration
     **/
    private final int redundanceRuns;

    /**
     * Stream to dump agent to
     **/
    private File agentDumpFile = null;

    /**
     * Dump for scores
     **/
    private Writer scoreDump = null;

    /**
     * Dump for win rates
     **/
    private Writer winRateDump = null;

    /**
     * Number of iterations between agent dumps
     **/
    private final int agentDumpInterval;

    /**
     * Progress, denoted by the current iteration
     **/
    private final AtomicInteger progress = new AtomicInteger(0);

    /**
     * Time the execution of this thread took
     *
     * It is set after the thread finished successfully.
     **/
    private final AtomicLong executionTime = new AtomicLong();

    /**
     * Current status of this thread
     *
     * Starts of in READY state.
     **/
    private State currentState = State.READY;

    /**
     * Creates a new worker thread.
     *
     * @param runnerName name of this runner, ought to be identifying
     * @param agent agent instance for this worker thread
     * @param iterations number of iterations (games to be played)
     * @param redundanceRuns number of repetitions per iteration
     * @param agentDump binary agent dump file
     * @param scoreDump CSV dump file for scores
     * @param winRateDump CSV dump file for win rates
     * @param agentDumpInterval number of iterations between agent dumps
     **/
    public WorkerThread(String runnerName, Agent agent, int iterations, int redundanceRuns, File agentDump, File scoreDump, File winRateDump, int agentDumpInterval) {
        // thread name
        super("Worker " + runnerName);

        // learning runs are not included => at least one redundance run
        ++redundanceRuns;

        this.runnerName = runnerName;
        this.agent = agent;
        this.iterations = iterations;
        this.redundanceRuns = redundanceRuns;
        this.agentDumpInterval = agentDumpInterval;

        // create dump files
        try {
            if (agentDump != null) {
                agentDump.createNewFile();
                this.agentDumpFile = agentDump;
            }

            if (scoreDump != null) {
                scoreDump.createNewFile();
                this.scoreDump = new BufferedWriter(new FileWriter(scoreDump));
            }

            if (winRateDump != null) {
                winRateDump.createNewFile();
                this.winRateDump = new BufferedWriter(new FileWriter(winRateDump));
            }
        } catch (IOException e) {
            System.err.println("Error opening dump file(s):");
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        long start = System.currentTimeMillis();

        synchronized (currentState) {
            assert currentState.equals(State.READY) : "worker thread was not in ready state";
            currentState = State.RUNNING;
        }

        // header for score dump
        if (scoreDump != null) {
            try {
                scoreDump.write("runnerName,iteration,averageScore\n");
            } catch (IOException e) {
                System.err.println("Error writing to dump file:");
                e.printStackTrace();
            }
        }

        // header for win rate dump
        if (winRateDump != null) {
            try {
                winRateDump.write("runnerName,iteration,averageWinRate\n");
            } catch (IOException e) {
                System.err.println("Error writing to dump file:");
                e.printStackTrace();
            }
        }

        for (int iteration = 0; iteration < iterations; ++iteration) {
            double averageScore = 0;
            double averageWinRate = 0;

            for (int run = 0; run < redundanceRuns; ++run) {
                Game game = new Game();
                agent.setGame(game);

                while (!game.isGameOver())
                    agent.playMove(false);

                averageScore += game.getPlayerScore();
                averageWinRate += game.isWon() ? 1 : 0;
            }

            averageScore /= (double) redundanceRuns;
            averageWinRate /= (double) redundanceRuns;

            // dump score
            if (scoreDump != null) {
                try {
                    scoreDump.write(runnerName + "," + iteration + "," + averageScore + "\n");
                } catch (IOException e) {
                    System.err.println("Error writing to dump file:");
                    e.printStackTrace();
                }
            }

            // dump win rate
            if (winRateDump != null) {
                try {
                    winRateDump.write(runnerName + "," + iteration + "," + averageWinRate + "\n");
                } catch (IOException e) {
                    System.err.println("Error writing to dump file:");
                    e.printStackTrace();
                }
            }

            // learning game
            Game game = new Game();
            agent.setGame(game);

            while (!game.isGameOver())
                agent.playMove(true);

            // dump agent
            if (iteration % agentDumpInterval == (agentDumpInterval - 1) && agentDumpFile != null) {
                try {
                    ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(agentDumpFile));
                    oos.writeObject(agent);
                    oos.close();
                } catch (IOException e) {
                    System.err.println("Error writing to dump file:");
                    e.printStackTrace();
                }
            }

            progress.incrementAndGet();
        }

        try {
            for (int i = 0; i < 3; ++i) {
                scoreDump.write("New Game\n");
                Game game = new Game();
                agent.setGame(game);

                while (!game.isGameOver()) {
                    agent.playMove(false);
                    scoreDump.write(game.toString() + "\n");
                }
            }
        } catch (IOException e) {
            System.err.println("Error closing dump file:");
            e.printStackTrace();
        }

        // close score dump
        if (scoreDump != null) {
            try {
                scoreDump.close();
            } catch (IOException e) {
                System.err.println("Error closing dump file:");
                e.printStackTrace();
            }
        }

        // close win rate dump
        if (winRateDump != null) {
            try {
                winRateDump.close();
            } catch (IOException e) {
                System.err.println("Error closing dump file:");
                e.printStackTrace();
            }
        }

        // close agent dump
        if (agentDumpFile != null) {
            try {
                ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(agentDumpFile));
                oos.writeObject(agent);
                oos.close();
            } catch (IOException e) {
                System.err.println("Error closing or writing to dump file:");
                e.printStackTrace();
            }
        }

        synchronized (currentState) {
            assert currentState.equals(State.RUNNING) : "worker thread was not in running state";
            currentState = State.FINISHED;
        }

        executionTime.set(System.currentTimeMillis() - start);
    }

    /**
     * Queries the current state of this worker thread.
     *
     * If the thread died while running, this function will set its state to FAILED.
     *
     * @return current state of this thread
     **/
    public State getCurrentState() {
        synchronized (currentState) {
            if (!isAlive() && currentState.equals(State.RUNNING))
                currentState = State.FAILED;

            return currentState;
        }
    }

    /**
     * Gets a number denoting the progress of execution.
     *
     * @return a number between 0.0 and 1.0 denoting progress
     **/
    public double getProgress() {
        return progress.doubleValue() / (double) iterations;
    }

    /**
     * Gets the milliseconds since start of execution.
     *
     * @return milliseconds since start of execution
     **/
    public long getExecutionTime() {
        return executionTime.longValue();
    }
}
