package main;

import static utils.Outputter.*;

import java.io.File;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Scanner;

import _2048.Game;
import _2048.MoveDirection;
import player.Agent;

public class Main {

    /**
     * Version number
     **/
    private static final double VERSION = 1.0;

    /**
     * Main driver.
     **/
    public static void main(String[] args) {
        println("");
        println("AURORA v" + VERSION);
        println("");

        handleArguments(args);
    }

    /**
     * Help message, informing about the usage of this program
     **/
    private static String helpMessage = String.join(System.lineSeparator(),
            "",
            "usage: java " + System.getProperty("sun.java.command") + " <command> [<args>]",
            "",
            "Following commands are available:",
            "  run <file...>           Execute all provided run configuration files",
            "  move <agent> <board>    Consult a trained agent on the current best move",
            "  agent <agent>           Watch a trained agent play one game",
            "  play                    Try your luck and give it a shot yourself"
            );

    /**
     * High level method to start routines based on program arguments.
     *
     * First argument specifies the opeartion to perform, there are:
     *   - run
     *   - agent
     *   - play
     *
     * @param arguments execution arguments that determines the actions to perform
     **/
    public static void handleArguments(String[] arguments) {
        if (arguments.length == 0) {
            System.out.println(helpMessage);
            return;
        }

        try {
            switch (arguments[0]) {
                case "run":
                    run(Arrays.stream(arguments).skip(1).map(File::new).toArray(File[]::new));
                    break;
                case "move": {
                        Agent agent = Agent.deserializeFromFile(new File(arguments[1]));
                        Game game = Game.fromString(arguments[2]);
                        if (agent != null) {
                            agent.setGame(game);
                            MoveDirection bestMove = agent.findBestMove();
                            if (bestMove == null) {
                                println("END");
                            } else {
                                println(bestMove.name());
                            }
                        }
                    }
                    break;
                case "agent": {
                        Agent agent = Agent.deserializeFromFile(new File(arguments[1]));
                        if (agent != null)
                            play(agent);
                    }
                    break;
                case "play":
                    play();
                    break;
                default:
                    System.out.println(helpMessage);
                    return;
            }
        } catch (InterruptedException e) {
            System.err.println("Thread got interrupted: " + e.getMessage());
        }
    }

    /**
     * Delay for playing agent, applied each move
     **/
    private static final int AGENT_DELAY_MS = 200;

    /**
     * Lets agent play a game of 2048, printing to stdout.
     *
     * @param agent agent to play
     **/
    public static void play(Agent agent) throws InterruptedException {
        String nextMove;
        Game g = new Game();
        agent.setGame(g);

        // print lines to compensate going lines up
        println(g);

        while (!g.isGameOver()) {
            // print move
            switch (agent.findBestMove()) {
                case DOWN:
                    nextMove = "ᐁ";
                    break;
                case LEFT:
                    nextMove = "ᐊ";
                    break;
                case RIGHT:
                    nextMove = "ᐅ";
                    break;
                case UP:
                    nextMove = "ᐃ";
                    break;
                default:
                    nextMove = "[]";
                    break;
            }
            println(CLEAR_LINE + "Agent's next move: " + nextMove);

            agent.playMove(false);
            Thread.sleep(AGENT_DELAY_MS);

            // print board
            goNLinesUp((int) g.toString().lines().count() + 2);
            println(g);
        }

        println(CLEAR_LINE + "Game over!");
    }

    /**
     * Reads from stdin and lets user play one game of 2048.
     **/
    public static void play() {
        Game g = new Game();
        Scanner sc = new Scanner(System.in);
        String line;

        // print move instructions
        println("h - left | j - down | k - up | l - right | q - quit");
        println("");
        println(g);
        println("Input your move: ");

        while (!g.isGameOver()) {

            try {
                line = sc.nextLine();
            } catch (NoSuchElementException e) {
                // EOF
                break;
            }

            if (line.length() == 0)
                continue;

            MoveDirection dir;
            switch (line.charAt(0)) {
                case 'h':
                    dir = MoveDirection.LEFT;
                    break;
                case 'j':
                    dir = MoveDirection.DOWN;
                    break;
                case 'k':
                    dir = MoveDirection.UP;
                    break;
                case 'l':
                    dir = MoveDirection.RIGHT;
                    break;
                case 'q':
                    return;
                default:
                    continue;
            }

            g.makeMove(dir);

            goNLinesUp((int) g.toString().lines().count() + 3);
            println(g);
            println("Input your move: ");
        }
        sc.close();

        System.out.println("Game over.");
    }

    /**
     * Builds worker threads from provided configurations and runs them.
     *
     * Additionally, also starts a progress thread for all worker threads.
     *
     * @param configFiles configuration INI files
     **/
    public static void run(File[] configFiles) throws InterruptedException {
        // create threads
        WorkerThread[] threads = Arrays.stream(configFiles)
            .filter(File::isFile)
            .map(WorkerThread::workerThreadsFromIniFile)
            .flatMap(Arrays::stream)
            .filter(f -> f != null)
            .toArray(WorkerThread[]::new);

        ProgressThread progress = new ProgressThread(threads);

        // start threads
        for (Thread thread : threads)
            thread.start();
        progress.start();

        // join threads
        for (Thread thread : threads)
            thread.join();
        progress.join();

        println("Done.");
    }
}
