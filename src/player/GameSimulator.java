package player;

import _2048.Game;
import _2048.MoveDirection;

/**
 * A class to simulate moves of a game instance
 **/
public class GameSimulator extends Game {

    /**
     * Parent game keeping track of the actual game state
     **/
    private Game parent;

    /**
     * Creates a new simulator with a given game as parent.
     *
     * @param game the parent game for this simulator
     **/
    public GameSimulator(Game game) {
        setParent(game);
    }

    /**
     * Creates a new simulator with a new game as parent.
     **/
    public GameSimulator() {
        this(new Game());
    }

    /**
     * Simulates a move on the parent board without new randomly generated tile.
     *
     * The resulting position is also known as the 'afterstate'.
     * Gained points by this move can be retrieved by the getLastReward method.
     *
     * @param direction the move direction to be simulated
     * @return legal move flag
     **/
    public boolean simulateMove(MoveDirection direction) {
        // synchronize fields from parent board
        copyFrom(parent);

        // simulate move
        return moveTiles(direction);
    }

    /**
     * Calculates the reward from last simulated move.
     *
     * @return reward from simulated move
     **/
    public int getLastReward() {
        return getPlayerScore() - parent.getPlayerScore();
    }

    /**
     * Gets the parent game.
     *
     * @return the current parent game
     **/
    public Game getParent() {
        return parent;
    }

    /**
     * Sets the parent game.
     *
     * @param game the new parent game
     **/
    public void setParent(Game game) {
        parent = game;
        copyFrom(parent);
    }
}
