package player;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

import _2048.Game;
import _2048.MoveDirection;
import ntn.NTupleNetwork;

/**
 * A class to play moves for the 2048 game with the ability to learn good moves
 **/
public class Agent implements Serializable {

    /**
     * Default serial version UID
     **/
    private static final long serialVersionUID = 1L;

    /**
     * Deserialize an agent from a binary file.
     *
     * @param file the binary file containing the agent
     * @return deserialized agent object from the file
     **/
    public static Agent deserializeFromFile(File file) {
        try {
            ObjectInputStream stream = new ObjectInputStream(new FileInputStream(file));
            Object agent = stream.readObject();

            if (agent instanceof Agent) {
                stream.close();
                return (Agent) agent;
            }

            stream.close();
        } catch (ClassNotFoundException | IOException e) {
            System.err.println("Error reading agent from binary file " + file.getName() + ": " + e.getMessage());
        }

        return null;
    }

    /**
     * The network used to evaluate positions
     **/
    private final NTupleNetwork network;

    /**
     * A value dictating the speed of learning good moves
     **/
    private double learningRate;

    /**
     * A simulator to compute afterstates of the game board
     **/
    private transient GameSimulator simulator = new GameSimulator();

    /**
     * Storage for best next move in case of 'in-advance' calculation
     **/
    private transient MoveDirection nextBestMove = null;

    /**
     * Creates a new agent with given values for network and learningRate.
     *
     * @param network the network used for dicision making and learning
     * @param learningRate the learning rate for this agent
     **/
    public Agent(NTupleNetwork network, double learningRate) {
        this.network = network;
        setLearningRate(learningRate);
    }

    /**
     * Determines best move for current game board based on afterstate calculation and network table lookup.
     *
     * @return 'best move' evaluation by this agent, null if no legal move possible (game over)
     **/
    public MoveDirection findBestMove() {
        MoveDirection bestMove = null;
        double bestValue = Double.NEGATIVE_INFINITY;

        // iterate through each move direction
        for (MoveDirection direction : MoveDirection.values()) {
            // skip illegal moves
            if (!simulator.simulateMove(direction))
                continue;

            // move value consists of network evaluation and reward
            double moveValue = simulator.getLastReward() + network.evaluate(simulator.getBoard());

            // compare with (old) best move
            if (moveValue > bestValue) {
                bestMove = direction;
                bestValue = moveValue;
            }
        }

        return bestMove;
    }

    /**
     * Last board state, used to learn based on future rewards
     **/
    private int[] oldBoard = new int[Game.BOARD_SIZE];

    /**
     * Plays one move as good as possible on current simulator.
     *
     * May train the network if specified.
     *
     * @param learningEnable if true, agent will learn from the played move
     * @return the move played by this agent
     **/
    public MoveDirection playMove(boolean learningEnable) {
        if (simulator.getParent().isGameOver())
            return null;

        boolean reuseSimulation = (nextBestMove != null);
        MoveDirection bestMove = reuseSimulation ? nextBestMove : findBestMove();
        nextBestMove = null;

        assert (bestMove != null) : "Game is not over, there must be a possible move";

        // if nextBestMove is set, learn() has already simulated the move
        if (!reuseSimulation)
            // play best known move
            simulator.simulateMove(bestMove);

        // save old board value for learning
        simulator.copyBoard(oldBoard);
        simulator.getParent().makeMove(bestMove);

        if (learningEnable)
            learn();

        return bestMove;
    }

    /**
     * Learn value of afterstate based on TD(0).
     *
     * Uses the oldBoard member saved by the playMove() method.
     * oldBoard contains the state of before the last move was played.
     **/
    private void learn() {
        nextBestMove = findBestMove();

        assert ((nextBestMove == null) == simulator.getParent().isGameOver()) : "Move can only not be found, iff game is over";

        // target approximation value for oldBoard board state
        // if game is over, learn 0 for old board
        double target = 0;

        // if game is not over, learn new value
        if (!simulator.getParent().isGameOver()) {
            simulator.simulateMove(nextBestMove);

            // new value consists of reward for best move + evaluation of resulting board
            target = simulator.getLastReward() + network.evaluate(simulator.getBoard());
        }

        // update value for board from before last move
        network.learn(oldBoard, target, learningRate);
    }

    /**
     * Gets the parent game from the simulator.
     *
     * @return the game currently played by this agent
     **/
    public Game getGame() {
        return simulator.getParent();
    }

    /**
     * Sets the parent game of the simulator.
     *
     * @param game the game to be played by this agent
     **/
    public void setGame(Game game) {
        simulator.setParent(game);
        nextBestMove = null;
    }

    /**
     * Sets the learning rate.
     *
     * @param learningRate the new learning rate
     **/
    public void setLearningRate(double learningRate) {
        this.learningRate = learningRate;
    }

    /**
     * Resets simulator field after deserialization.
     *
     * @return this object
     **/
    public Object readResolve() {
        simulator = new GameSimulator();

        return this;
    }
}
