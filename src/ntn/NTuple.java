package ntn;

import java.io.Serializable;

/**
 * A class to hold a variable amount of indices
 *
 * Capable of calculating LUT indices for given board states regarding a certain symmetry.
 * Every calculated LUT index is unique to the values at considered positions by this tuple.
 *
 * A LUT should be of size VALUE_EXPONENT_BOUND ^ N.
 **/
public class NTuple implements Serializable {

    /**
     * Default serial version UID
     **/
    private static final long serialVersionUID = 1L;

    /**
     * 2-exponent of greatest possible value
     **/
    public static final int VALUE_EXPONENT_BOUND = 16;

    /**
     * Transformation tables for rotation and reflection
     *
     * The first table (0) is the identity transformation.
     **/
    private static final int[][] SYMMETRIES_TABLE = {
        {
            0x0, 0x1, 0x2, 0x3,
            0x4, 0x5, 0x6, 0x7,
            0x8, 0x9, 0xA, 0xB,
            0xC, 0xD, 0xE, 0xF,
        }, {
            0x3, 0x7, 0xB, 0xF,
            0x2, 0x6, 0xA, 0xE,
            0x1, 0x5, 0x9, 0xD,
            0x0, 0x4, 0x8, 0xC,
        }, {
            0xF, 0xE, 0xD, 0xC,
            0xB, 0xA, 0x9, 0x8,
            0x7, 0x6, 0x5, 0x4,
            0x3, 0x2, 0x1, 0x0,
        }, {
            0xC, 0x8, 0x4, 0x0,
            0xD, 0x9, 0x5, 0x1,
            0xE, 0xA, 0x6, 0x2,
            0xF, 0xB, 0x7, 0x3,
        }, {
            0x3, 0x2, 0x1, 0x0,
            0x7, 0x6, 0x5, 0x4,
            0xB, 0xA, 0x9, 0x8,
            0xF, 0xE, 0xD, 0xC,
        }, {
            0xF, 0xB, 0x7, 0x3,
            0xE, 0xA, 0x6, 0x2,
            0xD, 0x9, 0x5, 0x1,
            0xC, 0x8, 0x4, 0x0,
        }, {
            0xC, 0xD, 0xE, 0xF,
            0x8, 0x9, 0xA, 0xB,
            0x4, 0x5, 0x6, 0x7,
            0x0, 0x1, 0x2, 0x3,
        }, {
            0x0, 0x4, 0x8, 0xC,
            0x1, 0x5, 0x9, 0xD,
            0x2, 0x6, 0xA, 0xE,
            0x3, 0x7, 0xB, 0xF,
        }
    };

    /**
     * Number of possible transformations (rotations and reflections) for a board
     **/
    public static final int SYMMETRIES = SYMMETRIES_TABLE.length;

    /**
     * Array holding n indices to a board
     *
     * Values at these board positions will be used for LUT index calculation.
     **/
    private final int[] indices;

    /**
     * Creates a new tuple, setting indices.
     *
     * Checks for illegal indices.
     *
     * @param indices the indices considered by this tuple
     **/
    public NTuple(int... indices) {
        this.indices = indices;

        // check for invalid index
        for (int index : indices) {
            if (index < 0 || index >= _2048.Game.BOARD_SIZE)
                throw new IllegalArgumentException("Index must not be greater or equal to " + _2048.Game.BOARD_SIZE + ", is " + index);
        }
    }

    /**
     * Calculates lookup-table index for given board state and symmetry.
     *
     * Only the values at positions considered by this tuple, applied to the symmetry, are used to compute the LUT index.
     * The same index can only be computed if all values in positions considered by this tuple are the same.
     * The calculated index is in range [0, VALUE_EXPONENT_BOUND ^ N).
     *
     * The symmetry (0) is the identity and does not change the board.
     *
     * @param board the board state to calculate the index from
     * @param symmetry symmetry index to be used
     * @return calculated LUT index
     **/
    public int calculateLUTIndex(int[] board, int symmetry) {
        int LUTIndex = 0;

        for (int index : indices) {
            // log2 of a given value on the board
            int exponent = getBoardValueExponent(board[SYMMETRIES_TABLE[symmetry][index]]);

            // exception if value is out of range
            if (exponent >= VALUE_EXPONENT_BOUND)
                throw new IllegalArgumentException("Illegal value in board tile: " + board[SYMMETRIES_TABLE[symmetry][index]] + " >= 2 ^ " + VALUE_EXPONENT_BOUND);

            // accumulate board value exponents to the index
            LUTIndex *= VALUE_EXPONENT_BOUND;
            LUTIndex += exponent;
        }

        return LUTIndex;
    }

    /**
     * Gets the number of considered positions.
     *
     * @return number of considered positions
     **/
    public int getN() {
        return indices.length;
    }

    /**
     * Calculates log2 of a power of 2.
     *
     * This method is not applicable for numbers that are not powers of 2.
     *
     * @param boardValue the value to calculate log2 of
     * @return log2 of the given value, 0 if given value is 0
     **/
    private static int getBoardValueExponent(int boardValue) {
        // let n be a power of 2 represented in binary: log2(n) = number of zeros after the one
        return boardValue == 0 ? 0 : Integer.numberOfTrailingZeros(boardValue);
    }
}
