package ntn;

import java.io.Serializable;
import java.util.Arrays;

/**
 * A class to hold LUTs and tuples in order to evaluate board positions
 *
 * Weights inside the LUTs can be adjusted to learn good positions.
 *
 * Symmetries, meaning rotations and reflections, of board positions can either be treaded differently or all as one.
 * This behavior has to be specified at instanciation time.
 **/
public class NTupleNetwork implements Serializable {

    /**
     * Default serial version UID
     **/
    private static final long serialVersionUID = 1L;

    /**
     * The tuples of this network
     *
     * They determine what tiles of a board to 'look' at.
     **/
    private final NTuple[] nTuples;

    /**
     * Array of LUTs, one for each tuple
     *
     * They hold the weights that determine the evaluation of a board position.
     * The LUTs are zero-initialized.
     **/
    private double[][] LUTs;

    /**
     * Flag for taking symmetries of board positions into account
     *
     * If true, rotations and reflections of a board will have the same index in the LUT.
     * Otherwise, every board state will have its unique weight in the LUT.
     **/
    private final boolean symmetricSampling;

    /**
     * Creates a network defined by a string.
     *
     * Following rules for the configuration string:
     *   - Tuples are to be seperated by a comma (',')
     *   - Positions captured by a tuple are to be seperated by a minus ('-')
     *   - Positions are denoted by integral values from 00 to 15
     *
     * @param symmetricSampling if enabled, the network will take symmetries of board positions into account for learning and evaluation
     * @param nTuples the configuration string for the network
     * @return a network following the structure of the arguments
     **/
    public NTupleNetwork(boolean symmetricSampling, String nTuples) {
        this(symmetricSampling, tuplesFromString(nTuples));
    }

    /**
     * Converts string representation of tuples to array representation.
     *
     * Following rules for the configuration string:
     *   - Tuples are to be seperated by a comma (',')
     *   - Positions captured by a tuple are to be seperated by a minus ('-')
     *   - Positions are denoted by integral values from 00 to 15
     *
     * @param nTuples the string representation of tuples
     * @return an array representation of tuples
     **/
    private static int[][] tuplesFromString(String nTuples) {
        int[][] tuples = new int[nTuples.split(",").length][];
        int index = 0;

        for (String tuple : nTuples.split(",")) {
            tuples[index] = Arrays.stream(tuple.split("-")).mapToInt(Integer::valueOf).toArray();

            // check for invalid indices
            for (int i = 0; i < tuples[index].length; ++i)
                if (tuples[index][i] < 0 || tuples[index][i] > 15)
                    throw new IndexOutOfBoundsException("Tuple string contains invalid index: " + i);

            ++index;
        }

        return tuples;
    }

    /**
     * Creates a new network.
     *
     * All weights are initialized to zero.
     *
     * @param symmetricSampling if enabled, the network will take symmetries of board positions into account for learning and evaluation
     * @param nTuples initialization arguments for each tuple
     **/
    public NTupleNetwork(boolean symmetricSampling, int[]... nTuples) {
        // throws if no tuples
        if (nTuples.length == 0)
            throw new IllegalArgumentException("Cannot instanciate network without tuples");

        this.nTuples = new NTuple[nTuples.length];
        this.symmetricSampling = symmetricSampling;

        for (int i = 0; i < getM(); ++i)
            this.nTuples[i] = new NTuple(nTuples[i]);

        // init tables
        initLUTs();
    }

    /**
     * Creates a new network.
     *
     * All weights are initialized to zero.
     *
     * @param symmetricSampling if enabled, the network will take symmetries of board positions into account for learning and evaluation
     * @param nTuples the tuples for this network
     **/
    public NTupleNetwork(boolean symmetricSampling, NTuple... nTuples) {
        // throws if no tuples
        if (nTuples.length == 0)
            throw new IllegalArgumentException("Cannot instanciate network without tuples");

        this.nTuples = nTuples;
        this.symmetricSampling = symmetricSampling;

        // init tables
        initLUTs();
    }

    /**
     * Calculates the positional value of a given board state.
     *
     * Each tuple calculates an index based on the given board state.
     * Every index points to a weight in a certain LUT.
     * The desired evaluation is the average of those weights.
     *
     * @param board the board state to be evaluated
     * @return an estimate of how good the position is
     **/
    public double evaluate(int[] board) {
        double value = 0;

        // iterate through all symmetries
        for (int symmetry = 0; symmetry < NTuple.SYMMETRIES; ++symmetry) {
            // sum up values from LUTs of each tuple
            for (int tuple = 0; tuple < getM(); ++tuple)
                value += LUTs[tuple][nTuples[tuple].calculateLUTIndex(board, symmetry)];

            // return if only identity symmetry should be used
            if (!symmetricSampling)
                return value;
        }

        return value;
    }

    /**
     * Changes weights of LUTs for all tuples for a given board state.
     *
     * @param board board state for which the weights should be adjusted
     * @param target the target value for all tuples
     * @param learningRate a factor that determines the learning speed
     **/
    public void learn(int[] board, double target, double learningRate) {
        // subtract existing approximation
        double delta = target - evaluate(board);

        // iterate through all symmetries
        for (int symmetry = 0; symmetry < NTuple.SYMMETRIES; ++symmetry) {
            for (int tuple = 0; tuple < getM(); ++tuple)
                LUTs[tuple][nTuples[tuple].calculateLUTIndex(board, symmetry)] += learningRate * delta;

            // return if only identity symmetry should be learned
            if (!symmetricSampling)
                return;
        }
    }

    /**
     * Gets the number of tuples in this network.
     *
     * @return number of tuples in this network
     **/
    public int getM() {
        return nTuples.length;
    }

    /**
     * Initializes an empty array as LUT for each tuple.
     *
     * LUT sizes may differ, but should always be large enough to contain all weights needed.
     *
     * The nTuples field must be set correctly before the call to this method.
     **/
    private void initLUTs() {
        LUTs = new double[getM()][];

        // size of LUT: VALUE_EXPONENT_BOUND ^ N
        for (int i = 0; i < getM(); ++i)
            LUTs[i] = new double[(int) Math.pow(NTuple.VALUE_EXPONENT_BOUND, nTuples[i].getN())];
    }
}
