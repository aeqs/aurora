package _2048;

/**
 * Random number generator used by game
 **/
public interface RandomNumberGenerator {

    /**
     * Returns a random integer each call.
     *
     * Resulting integer must be in the interval [0,n).
     *
     * @param n upper bound (exclusive)
     * @return random integer value
     **/
    int nextInt(int n);
}
