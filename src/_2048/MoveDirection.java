package _2048;

/**
 * An enum listing all four possible move directions
 **/
public enum MoveDirection {
    DOWN,
    LEFT,
    RIGHT,
    UP,
}
