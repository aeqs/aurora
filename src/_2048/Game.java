package _2048;

import java.util.concurrent.ThreadLocalRandom;

/**
 * A class to simulate the game 2048
 **/
public class Game {

    /**
     * Number of tiles on the board
     **/
    public static final int BOARD_SIZE = 16;

    /**
     * Value representing an empty tile
     **/
    private static final int EMPTY_CELL = 0;

    /**
     * Random number generator
     **/
    private final RandomNumberGenerator rng;

    /**
     * Represention of current board state, zero-initialized
     **/
    private int[] tiles = new int[BOARD_SIZE];

    /**
     * Player's points
     **/
    private int playerScore = 0;

    /**
     * Number of empty tiles on the board
     *
     * Initialized to number of tiles.
     **/
    private int freeTiles = BOARD_SIZE;

    /**
     * 'Game over' flag
     *
     * true if game is over, false otherwise.
     **/
    private boolean gameOver = false;

    /**
     * 'has won' flag
     *
     * true if 2048 tile has been reached, false otherwise.
     **/
    private boolean won = false;

    /**
     * Creates a game with board described by the parameter.
     *
     * The board string contains the values of the tiles, separated by a single whitespace.
     * For each tile, its desired exponent should be in the string.
     * A '0' in the string denotes an empty tile.
     *
     * @param board string representation of the board
     * @return a game instance with desired board
     **/
    public static Game fromString(String board) {
        Game game = new Game();

        String[] values = board.trim().split(" ");

        for (int i = 0; i < BOARD_SIZE; ++i)
            game.tiles[i] = 0;

        for (int i = 0; i < BOARD_SIZE; ++i) {
            if (i == values.length)
                break;

            int value = Integer.valueOf(values[i]);
            if (value == 0)
                continue;

            game.tiles[i] = 1 << value;
            --game.freeTiles;
            if (game.tiles[i] >= 2048)
                game.won = true;
        }

        return game;
    }

    /**
     * Creates a new game.
     **/
    public Game() {
        this(ThreadLocalRandom.current()::nextInt);
    }

    /**
     * Creates a new game with custom random number generator.
     **/
    public Game(RandomNumberGenerator rng) {
        this.rng = rng;

        init();
    }

    /**
     * Tries moving tiles in given direction.
     *
     * Generates new tile if move was legal.
     * Game remains unchanged if move was illegal.
     *
     * @param direction the direction to move the tiles in
     * @return true if move was legal, false otherwise
     **/
    public boolean makeMove(MoveDirection direction) {
        // return if game is already over
        if (gameOver)
            return false;

        if (moveTiles(direction)) {
            // spawn new tile
            generateRandomTile();

            // check for game over, i.e. no free tiles and merges left
            if (freeTiles == 0 && !canMerge())
                gameOver = true;

            return true;
        }

        return false;
    }

    /**
     * Copies the current board into an already existing array.
     *
     * @param dest the array the board is copied into
     **/
    public void copyBoard(int[] dest) {
        if (dest.length != tiles.length)
            throw new RuntimeException("Dest length <" + dest.length + "> is not the same as tiles length <" + tiles.length + ">");

        System.arraycopy(tiles, 0, dest, 0, BOARD_SIZE);
    }

    /**
     * Gets a reference to the current board.
     *
     * @return a reference to the board
     **/
    public int[] getBoard() {
        return tiles;
    }

    /**
     * Gets the player's score.
     *
     * @return the player's score
     **/
    public int getPlayerScore() {
        return playerScore;
    }

    /**
     * Checks if game has finished yet.
     *
     * @return true if game is over, false otherwise
     **/
    public boolean isGameOver() {
        return gameOver;
    }

    /**
     * Checks if game has been won.
     *
     * @return true if 2048 tile has been reached, false otherwise
     **/
    public boolean isWon() {
        return won;
    }

    /**
     * Converts current board state into a 2D-readable string.
     *
     * The indices of stringified tiles go left-to-right and bottom-up.
     *
     * @return a string representation of current board state
     **/
    public String boardToString() {
        String s = "+-------+-------+-------+-------+\n";

        for (int i = 3; i >= 0; --i) {
            s += "|       |       |       |       |\n";

            for (int j = 0; j < 4; ++j) {
                // leave empty for empty tiles, numeric value otherwise
                if (getTileAt(j, i) == EMPTY_CELL)
                    s += "|       ";
                else
                    s += String.format("| %5s ", getTileAt(j, i));
            }

            s += "|\n|       |       |       |       |\n";
            s += "+-------+-------+-------+-------+";

            if (i != 0)
                s += "\n";
        }

        return s;
    }

    /**
     * Stringifies Game instance.
     *
     * @return stringified Game state
     **/
    @Override
    public String toString() {
        // stringify player's score and the board
        return "Score: " + playerScore + "\nBoard:\n" + boardToString();
    }

    /**
     * Copies all fields from another game instance.
     *
     * Note: the random number generator will not be copied.
     *
     * @param other game instance to copy fields from
     **/
    protected void copyFrom(Game other) {
        other.copyBoard(tiles);
        freeTiles   = other.freeTiles;
        playerScore = other.playerScore;
        gameOver    = other.gameOver;
        won         = other.won;
    }

    /**
     * Spawns tiles on start of game.
     **/
    private void init() {
        // generate two tiles
        generateRandomTile();
        generateRandomTile();
    }

    /**
     * Puts a new tile in a randomly chosen, empty tile position.
     *
     * This method requires freeTiles to be set correctly in order to function properly.
     **/
    private void generateRandomTile() {
        // a random amount of empty tiles to be skipped
        int skips = rng.nextInt(freeTiles);

        // skip n (skips) empty tiles and put random tile after
        for (int i = 0; i < BOARD_SIZE; ++i) {
            if (tiles[i] == EMPTY_CELL) {
                if (skips == 0) {
                    // put new tile at index i
                    putNewTileAt(i);
                    return;
                }

                --skips;
            }
        }

        // skips must be less than freeTiles
        assert false : "skips has a value greater or equal to freeTiles";
    }

    /**
     * Puts a new tile at a given index.
     *
     * 90% chance 2-tile
     * 10% chance 4-tile
     *
     * @param i the index to put the new tile at
     **/
    private void putNewTileAt(int i) {
        // reduce freeTiles if put on an empty tile
        if (tiles[i] == EMPTY_CELL)
            --freeTiles;

        // 0.9 chance new tile is 2
        // 0.1 chance new tile is 4
        if (rng.nextInt(10) < 9)
            tiles[i] = 2;
        else
            tiles[i] = 4;
    }

    /**
     * Checks if a merge is possible without sliding.
     *
     * @return true if merge is possible without sliding, false otherwise
     **/
    private boolean canMerge() {
        for (int i = 0; i < 4; ++i) {
            for (int j = 0; j < 3; ++j) {
                // check horizontally
                if (getTileAt(j, i) == getTileAt(j + 1, i))
                    return true;

                // check vertically
                if (getTileAt(i, j) == getTileAt(i, j + 1))
                    return true;
            }
        }

        // no merges are possible
        return false;
    }

    /**
     * Moves the tiles in a given direction accordingly to game rules.
     *
     * Sliding and merging tiles if necessary.
     *
     * @param direction move direction to move tiles in
     * @return true if the board changed, false otherwise
     **/
    protected boolean moveTiles(MoveDirection direction) {
        boolean boardChanged = false;

        boardChanged |= slideTiles(direction);
        boardChanged |= mergeTiles(direction);
        // second slide is needed for special case: ABBA
        boardChanged |= slideTiles(direction);

        return boardChanged;
    }


    /**
     * Slides all tiles in a given direction until they hit a wall or another tile.
     *
     * The slide order is dependent on move direction, too.
     *
     * @param direction move direction to slide tiles in
     * @return true if the board changed, false otherwise
     **/
    private boolean slideTiles(MoveDirection direction) {
        boolean boardChanged = false;

        // iterates through rows/columns dependent on dir
        for (int i = 0; i < 4; ++i) {
            // the amount of stacked (consecutively touching a wall) tiles at a time
            // resets for each row/column
            int stackSize = 0;

            for (int j = 0; j < 3; ++j) {
                // tile index for top of stack
                int top;
                // current tile index we are looking at
                int tile;

                // set tile and top accordingly to dir
                switch (direction) {
                    case DOWN:
                        top = stackSize * 4 + i;
                        tile = (j + 1) * 4 + i;
                        break;
                    case LEFT:
                        top = i * 4 + stackSize;
                        tile = i * 4 + j + 1;
                        break;
                    case RIGHT:
                        top = i * 4 + (3 - stackSize);
                        tile = i * 4 + (2 - j);
                        break;
                    case UP:
                        top = (3 - stackSize) * 4 + i;
                        tile = (2 - j) * 4 + i;
                        break;
                    default:
                        throw new RuntimeException("Invalid MoveDirection was provided: " + direction);
                }

                // adjust stack based on the existance of tiles
                // and move the tile accordingly (slide)
                if (tiles[top] == EMPTY_CELL) {
                    if (tiles[tile] != EMPTY_CELL) {
                        // slide
                        tiles[top] = tiles[tile];
                        tiles[tile] = EMPTY_CELL;
                        ++stackSize;
                        boardChanged = true;
                    }
                } else {
                    ++stackSize;
                }
            }
        }

        return boardChanged;
    }

    /**
     * Merges all adjacent tiles with same value, regarding a certain direction, to the next bigger value.
     *
     * The merge order is dependent on move direction, too.
     * This method also updates the player score and checks for a win.
     *
     * @param direction move direction to merge tiles in
     * @return true if the board changed, false otherwise
     **/
    private boolean mergeTiles(MoveDirection direction) {
        boolean boardChanged = false;

        // iterate through each tile pair
        for (int i = 0; i < 4; ++i) {
            for (int j = 0; j < 3; ++j) {
                // the two tiles (indices) beeing compared, near beeing more in the 'direction' than far
                int near, far;

                // set near and far accordingly to direction
                switch (direction) {
                    case DOWN:
                        near = j * 4 + i;
                        far = (j + 1) * 4 + i;
                        break;
                    case LEFT:
                        near = i * 4 + j;
                        far = i * 4 + j + 1;
                        break;
                    case RIGHT:
                        near = i * 4 + (3 - j);
                        far = i * 4 + (2 - j);
                        break;
                    case UP:
                        near = (3 - j) * 4 + i;
                        far = (2 - j) * 4 + i;
                        break;
                    default:
                        throw new RuntimeException("Invalid MoveDirection was provided: " + direction);
                }

                // if tiles match and are not empty, merge together
                if (tiles[near] != EMPTY_CELL && tiles[near] == tiles[far]) {
                    tiles[near] *= 2;
                    tiles[far] = 0;
                    ++freeTiles;
                    // add points to score
                    playerScore += tiles[near];
                    boardChanged = true;

                    if (tiles[near] == 2048)
                        won = true;
                }
            }
        }
        return boardChanged;
    }

    /**
     * Gets the value at x-y-coordinate of the board.
     *
     * @return value for tile at x-y-coordinate
     **/
    private int getTileAt(int x, int y) {
        return tiles[y * 4 + x];
    }
}
