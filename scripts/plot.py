import os
import pandas as pd
import matplotlib.pyplot as plt

stats_dir = 'stats'
plots_dir = 'plots'

"""
Plot every file in stats dir
"""
for file_name in os.listdir(stats_dir):
    """
    Read CSV stats file
    """
    df = pd.read_csv(os.path.join(stats_dir, file_name))
    window_size = 5000

    x = 'iteration'
    y = ''
    plt.figure(figsize = (10, 6))

    """
    Differentiate between score and win rate stats
    """
    if file_name.endswith('_wr.csv'):
        y = 'averageWinRate'
        plt.xlabel('Iteration')
        plt.ylabel('Average Win Rate')
        plt.title(f'Average Win Rate of Agent {file_name.rsplit("_", 1)[0]}')
        plt.ylim(0, 1)

    elif file_name.endswith('_sc.csv'):
        y = 'averageScore'
        plt.xlabel('Iteration')
        plt.ylabel('Average Score')
        plt.title(f'Average Score of Agent {file_name.rsplit("_", 1)[0]}')

    else:
        print(f'skipping file {file_name}')
        continue

    """
    Make plot 'smoother'
    """
    df['windowed'] = df[y].rolling(window = window_size).mean()

    """
    Plot and save
    """
    plt.plot(df[x].values, df['windowed'].values)
    plt.grid(True)
    plt.savefig(os.path.join(plots_dir, f'{file_name.split(".", 1)[0]}.png'), format="png", dpi = 300)
    plt.close()
