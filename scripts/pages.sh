#!/bin/sh

echo '<html><head><style>body{display:flex;flex-wrap:wrap;}img{width:50%;height:auto;}</style></head><body>' > public/index.html

for plot in plots/*.png
do
  filename=$(basename $plot)

  echo "<img src='$filename'/><br/>" >> public/index.html
  cp $plot public/
done

echo '</body></html>' >> public/index.html
