package ntn;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

/**
 * Test behavior of NTupleNetwork, doesn't test learning progress
 **/
public class NTupleNetworkTest {

  private NTupleNetwork ntn;

  @Before
  public void init4TNFullyConnected() {
    // all 4-tuples horizontal/vertical lines
    // and 2x2 squares
    int[][] nTuples = {
      { 0, 1, 2, 3 },
      { 4, 5, 6, 7 },
      { 8, 9, 10, 11 },
      { 12, 13, 14, 15 },

      { 0, 4, 8, 12 },
      { 1, 5, 9, 13 },
      { 2, 6, 10, 14 },
      { 3, 7, 12, 15 },

      { 0, 1, 4, 5 },
      { 1, 2, 5, 6 },
      { 2, 3, 6, 7 },
      { 4, 5, 8, 9 },
      { 5, 6, 9, 10 },
      { 6, 7, 10, 11 },
      { 8, 9, 12, 13 },
      { 9, 10, 13, 14 },
      { 10, 11, 14, 15 }
    };
    ntn = new NTupleNetwork(false, nTuples);
  }

  @Test
  public void MAfterInit() {
    assertEquals(17, ntn.getM());
  }

  @Test
  public void nullInitEvalComplexBoard() {
    int[] in = {
      16, 0, 1024, 0,
      8, 0, 2, 256,
      2, 2, 8, 0,
      2, 4, 8, 4,
    };

    assertEquals(0, ntn.evaluate(in), 0);
  }

  @Test
  public void learningChangesEval() {
    int[] in = {
      16, 0, 1024, 0,
      8, 0, 2, 256,
      2, 2, 8, 0,
      2, 4, 8, 4,
    };

    double before = ntn.evaluate(in);
    double after;

    ntn.learn(in, 1, 1);
    after = ntn.evaluate(in);
    assertEquals(false, before == after);

    ntn.learn(in, 1.5, 1);
    after = ntn.evaluate(in);
    assertEquals(false, before == after);

    ntn.learn(in, 0, 1);
    after = ntn.evaluate(in);
    assertEquals(false, before == after);
  }
}
