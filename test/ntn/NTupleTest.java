package ntn;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

/**
 * Test for NTuple class
 **/
public class NTupleTest {

    @Test
    public void getN() {
        assertEquals(4, new NTuple(2, 3, 7, 5).getN());
        assertEquals(0, new NTuple().getN());
        assertEquals(3, new NTuple(15, 4, 2).getN());
        assertEquals(8, new NTuple(4, 2, 9, 5, 3, 1, 10, 13).getN());
    }

    // first few LUTI tests are without symmetry
    @Test
    public void calcLUTIdxNonZero() {
        NTuple nt = new NTuple(0, 1, 4, 5);
        int[] in = {
            2, 4, 0, 0,
            256, 8, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0
        };

        assertEquals(false, nt.calculateLUTIndex(in, 0) == 0);
    }

    @Test
    public void uniqueLUTIdxs() {
        NTuple nt = new NTuple(0, 1);
        Set<Integer> usedIndices = new HashSet<>();

        for (int i = 0; i < NTuple.VALUE_EXPONENT_BOUND; ++i) {
            for (int j = 0; j < NTuple.VALUE_EXPONENT_BOUND; ++j) {
                int[] in = { (int) Math.pow(2, i), (int) Math.pow(2, j) };
                int index = nt.calculateLUTIndex(in, 0);

                assertEquals(false, usedIndices.contains(index));
                usedIndices.add(index);
            }
        }
    }

    @Test
    public void calcLUTIdxAllZero() {
        NTuple nt = new NTuple(0, 1, 4, 5);
        int[] in = {
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0
        };

        assertEquals(0, nt.calculateLUTIndex(in, 0));
    }

    @Test
    public void calcLUTIdxNoOthers() {
        NTuple nt = new NTuple(0, 1, 4, 5);
        int[] in = {
            0, 0, 4, 4,
            0, 0, 4, 4,
            4, 4, 4, 4,
            4, 4, 4, 4
        };

        assertEquals(0, nt.calculateLUTIndex(in, 0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExceptionOnBoardValueTooBig() {
        NTuple nt = new NTuple(0);
        int[] in = { (int) Math.pow(2, NTuple.VALUE_EXPONENT_BOUND) };

        nt.calculateLUTIndex(in, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void initIndexTooLarge() {
        new NTuple(16);
    }

    @Test(expected = IllegalArgumentException.class)
    public void initIndexTooSmall() {
        new NTuple(-1);
    }

    @Test
    public void initIndexLarge() {
        new NTuple(15);
    }

    @Test
    public void initIndexSmall() {
        new NTuple(0);
    }

    @Test
    public void symmetries() {
        NTuple nt = new NTuple(0);
        int[] in = {
            32, 0, 0, 4,
            0, 0, 0, 0,
            0, 0, 0, 0,
            16, 0, 0, 8
        };
        int[] seen = new int[NTuple.VALUE_EXPONENT_BOUND];

        for (int i = 0; i < NTuple.SYMMETRIES; ++i)
            ++seen[nt.calculateLUTIndex(in, i)];

        int differentIdxs = 0;
        for (int i = 0; i < NTuple.VALUE_EXPONENT_BOUND; ++i) {
            if (seen[i] == 0)
                continue;

            // two times seen, due to reflections
            assertEquals(2, seen[i]);
            ++differentIdxs;
        }

        // four different indices, due to rotations
        assertEquals(4, differentIdxs);
    }
}
