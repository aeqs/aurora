package player;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
import _2048.Game;
import _2048.MoveDirection;
import _2048.PredeterminedRNG;

/**
 * Test GameSimulator class
 **/
public class GameSimulatorTest {

    @Test
    public void parentIsSameAsOrig() {
        Game g = new Game();
        GameSimulator gs = new GameSimulator(g);

        // guarantee at least one board change
        g.makeMove(MoveDirection.DOWN);
        g.makeMove(MoveDirection.UP);

        assertArrayEquals(g.getBoard(), gs.getParent().getBoard());
        assertEquals(g, gs.getParent());
    }

    @Test
    public void setParentSameAsGetParent() {
        Game g = new Game();
        GameSimulator gs = new GameSimulator();

        gs.setParent(g);
        assertEquals(g, gs.getParent());
    }

    @Test
    public void lastRewardInit() {
        assertEquals(0, new GameSimulator().getLastReward());
        assertEquals(0, new GameSimulator(new Game()).getLastReward());
    }

    @Test
    public void lastRewardMerge() {
        Game g = new Game(new PredeterminedRNG(0, 0, 0, 0)); // two 2's next to each other
        GameSimulator gs = new GameSimulator(g);

        gs.simulateMove(MoveDirection.LEFT);

        assertEquals(4, gs.getLastReward());
    }

    @Test
    public void lastRewardIlleagalMove() {
        Game g = new Game(new PredeterminedRNG(0, 0, 0, 0)); // all tiles on bottom
        GameSimulator gs = new GameSimulator(g);

        gs.simulateMove(MoveDirection.DOWN);

        assertEquals(0, gs.getLastReward());
    }

    @Test
    public void lastRewardNoMerge() {
        Game g = new Game(new PredeterminedRNG(0, 9, 0, 0)); // 4 and 2 next to each other
        GameSimulator gs = new GameSimulator(g);

        gs.simulateMove(MoveDirection.RIGHT);

        assertEquals(0, gs.getLastReward());
    }

    @Test
    public void simulateIllegalMove() {
        Game g = new Game(new PredeterminedRNG(0, 0, 0, 0)); // all tiles on bottom
        GameSimulator gs = new GameSimulator(g);

        assertEquals(false, gs.simulateMove(MoveDirection.DOWN));
    }

    @Test
    public void simulateLegalMove() {
        Game g = new Game(new PredeterminedRNG(0, 0, 0, 0)); // all tiles on bottom
        GameSimulator gs = new GameSimulator(g);

        assertEquals(true, gs.simulateMove(MoveDirection.UP));
    }
}
