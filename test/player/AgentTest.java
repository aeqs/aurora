package player;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import _2048.Game;
import _2048.MoveDirection;
import _2048.PredeterminedRNG;
import ntn.NTupleNetwork;

/**
 * Test class Agent
 *
 * Due to complicated learning algorithm, not many test can be done.
 **/
public class AgentTest {

    private Agent a;

    @Before
    public void initFullyConnected() {
        int[][] nTuples = {
            { 0, 1, 2, 3 },
            { 4, 5, 6, 7 },
            { 8, 9, 10, 11 },
            { 12, 13, 14, 15 },

            { 0, 4, 8, 12 },
            { 1, 5, 9, 13 },
            { 2, 6, 10, 14 },
            { 3, 7, 11, 15 },

            { 0, 1, 4, 5 },
            { 1, 2, 5, 6 },
            { 2, 3, 6, 7 },
            { 4, 5, 8, 9 },
            { 5, 6, 9, 10 },
            { 6, 7, 10, 11 },
            { 8, 9, 12, 13 },
            { 9, 10, 13, 14 },
            { 10, 11, 14, 15 }
        };

        a = new Agent(new NTupleNetwork(false, nTuples), 0);
    }

    @Test
    public void dumbFindBestMoveHori() {
        Game g = new Game(new PredeterminedRNG(0, 0, 0, 0)); // two 2's next to each other

        a.setGame(g);
        MoveDirection md = a.findBestMove();

        assertTrue(md == MoveDirection.LEFT || md == MoveDirection.RIGHT);
    }

    @Test
    public void dumbFindBestMoveVerti() {
        Game g = new Game(new PredeterminedRNG(0, 0, 3, 0)); // two 2's on top of each other

        a.setGame(g);
        MoveDirection md = a.findBestMove();

        assertEquals(true, md == MoveDirection.UP || md == MoveDirection.DOWN);
    }

    @Test
    public void serializedSameAsTrained() throws IOException {
        Random gen = new Random(0);
        a.setLearningRate(0.0025);

        for (int i = 0; i < 100; ++i) {
            Game g = new Game(gen::nextInt);
            a.setGame(g);

            while (!g.isGameOver())
                a.playMove(true);
        }

        File tempFile = File.createTempFile("AgentSerialization-", ".bin");
        tempFile.deleteOnExit();
        ObjectOutputStream tempDump = new ObjectOutputStream(new FileOutputStream(tempFile));

        tempDump.writeObject(a);
        Agent a2 = Agent.deserializeFromFile(tempFile);

        for (int i = 0; i < 100; ++i) {
            Game g = new Game();
            a.setGame(g);

            while (!g.isGameOver()) {
                a2.setGame(g);
                assertEquals(a.findBestMove(), a2.findBestMove());
                a.playMove(false);
            }
        }

        tempDump.close();
    }
}
