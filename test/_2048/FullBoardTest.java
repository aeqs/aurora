package _2048;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

/**
 * Test behavior for game over
 **/
public class FullBoardTest {

    private Game game;
    private int preScore;

    @Before
    public void setupGame() {
        PredeterminedRNG prng = new PredeterminedRNG(
                4, 0, 4, 9, 4, 0, 4, 9,
                4, 9, 4, 0, 4, 9, 4, 0,
                4, 0, 4, 9, 4, 0, 4, 9,
                1, 9, 1, 0, 1, 9, 0, 0
                ); // white-box knowledge
        game = new Game(prng);

        game.makeMove(MoveDirection.DOWN);
        game.makeMove(MoveDirection.DOWN);
        game.makeMove(MoveDirection.DOWN);
        game.makeMove(MoveDirection.DOWN);
        game.makeMove(MoveDirection.DOWN);
        game.makeMove(MoveDirection.DOWN);
        game.makeMove(MoveDirection.DOWN);
        game.makeMove(MoveDirection.DOWN);
        game.makeMove(MoveDirection.DOWN);
        game.makeMove(MoveDirection.DOWN);
        game.makeMove(MoveDirection.DOWN);
        game.makeMove(MoveDirection.LEFT);
        game.makeMove(MoveDirection.LEFT);
        preScore = game.getPlayerScore();

        // if next move left, board is full
    }

    @Test
    public void setup() {
        int[] cmp = {
            2, 4, 2, 4,
            4, 2, 4, 2,
            2, 4, 2, 4,
            4, 2, 0, 4,
        };

        assertArrayEquals(cmp, game.getBoard());
        assertEquals(0, preScore);
        assertEquals(false, game.isWon());
        assertEquals(false, game.isGameOver());
    }

    @Test
    public void gameOver() {
        boolean legal = game.makeMove(MoveDirection.LEFT);
        int scoreDiff = game.getPlayerScore() - preScore;

        int[] cmp2 = {
            2, 4, 2, 4,
            4, 2, 4, 2,
            2, 4, 2, 4,
            4, 2, 4, 2,
        };

        assertArrayEquals(cmp2, game.getBoard());
        assertEquals(true, legal);
        assertEquals(0, scoreDiff);
        assertEquals(false, game.isWon());
        assertEquals(true, game.isGameOver());
    }

    @Test
    public void noChangeAfterGameOver() {
        game.makeMove(MoveDirection.LEFT);
        // Game Over

        boolean legal = false;
        legal |= game.makeMove(MoveDirection.LEFT);
        legal |= game.makeMove(MoveDirection.RIGHT);
        legal |= game.makeMove(MoveDirection.UP);
        legal |= game.makeMove(MoveDirection.DOWN);
        int scoreDiff = game.getPlayerScore() - preScore;

        int[] cmp = {
            2, 4, 2, 4,
            4, 2, 4, 2,
            2, 4, 2, 4,
            4, 2, 4, 2,
        };

        assertArrayEquals(cmp, game.getBoard());
        assertEquals(false, legal);
        assertEquals(0, scoreDiff);
        assertEquals(false, game.isWon());
        assertEquals(true, game.isGameOver());
    }
}
