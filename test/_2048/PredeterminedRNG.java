package _2048;

/**
 * A not so random random number generator
 *
 * Simply spits out a sequence of numbers.
 **/
public class PredeterminedRNG implements RandomNumberGenerator {

    private final int[] seq;

    private int pos = 0;

    public PredeterminedRNG(int... seq) {
        this.seq = seq;
    }

    /**
     * Returns next number of sequence.
     **/
    @Override
    public int nextInt(int n) {
        if (pos >= seq.length)
            return 0;

        return seq[pos++];
    }
}
