package _2048;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

/**
 * Test for board, score, won, and legal in each direction
 **/
public class ModifyGameTest {

    private Game game;
    private int preScore;

    @Before
    public void setupGame() {
        PredeterminedRNG prng = new PredeterminedRNG(0, 9, 3, 9, 4, 0, 4, 0, 4, 0, 5, 9, 7); // white-box knowledge
        game = new Game(prng);

        game.makeMove(MoveDirection.DOWN);
        game.makeMove(MoveDirection.DOWN);
        game.makeMove(MoveDirection.DOWN);
        game.makeMove(MoveDirection.DOWN);
        game.makeMove(MoveDirection.DOWN);
        preScore = game.getPlayerScore();

        // next move: first 0 will fill with 2
    }

    @Test
    public void setup() {
        int[] cmp = {
            8, 2, 2, 2,
            0, 4, 0, 0,
            0, 0, 0, 0,
            2, 0, 0, 0,
        };

        assertArrayEquals(cmp, game.getBoard());
        assertEquals(8, preScore);
        assertEquals(false, game.isWon());
        assertEquals(false, game.isGameOver());
    }

    @Test
    public void left() {
        boolean legal = game.makeMove(MoveDirection.LEFT);
        int scoreDiff = game.getPlayerScore() - preScore;

        int[] cmp = {
            8, 4, 2, 2,
            4, 0, 0, 0,
            0, 0, 0, 0,
            2, 0, 0, 0,
        };

        assertArrayEquals(cmp, game.getBoard());
        assertEquals(true, legal);
        assertEquals(4, scoreDiff);
        assertEquals(false, game.isWon());
        assertEquals(false, game.isGameOver());
    }

    @Test
    public void right() {
        boolean legal = game.makeMove(MoveDirection.RIGHT);
        int scoreDiff = game.getPlayerScore() - preScore;

        int[] cmp = {
            2, 8, 2, 4,
            0, 0, 0, 4,
            0, 0, 0, 0,
            0, 0, 0, 2,
        };

        assertArrayEquals(cmp, game.getBoard());
        assertEquals(true, legal);
        assertEquals(4, scoreDiff);
        assertEquals(false, game.isWon());
        assertEquals(false, game.isGameOver());
    }

    @Test
    public void up() {
        boolean legal = game.makeMove(MoveDirection.UP);
        int scoreDiff = game.getPlayerScore() - preScore;

        int[] cmp = {
            2, 0, 0, 0,
            0, 0, 0, 0,
            8, 2, 0, 0,
            2, 4, 2, 2,
        };

        assertArrayEquals(cmp, game.getBoard());
        assertEquals(true, legal);
        assertEquals(0, scoreDiff);
        assertEquals(false, game.isWon());
        assertEquals(false, game.isGameOver());
    }

    @Test
    public void down() {
        boolean legal = game.makeMove(MoveDirection.DOWN);
        int scoreDiff = game.getPlayerScore() - preScore;

        int[] cmp = {
            8, 2, 2, 2,
            2, 4, 2, 0,
            0, 0, 0, 0,
            0, 0, 0, 0,
        };

        assertArrayEquals(cmp, game.getBoard());
        assertEquals(true, legal);
        assertEquals(0, scoreDiff);
        assertEquals(false, game.isWon());
        assertEquals(false, game.isGameOver());
    }
}
