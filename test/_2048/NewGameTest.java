package _2048;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

/**
 * Test newly instanciated game
 **/
public class NewGameTest {

    private Game game;

    @Before
    public void setupNewGame() {
        game = new Game();
    }

    @Test
    public void score() {
        assertEquals(0, game.getPlayerScore());
    }

    @Test
    public void numberOfTiles() {
        int tiles = 0;

        for (int i : game.getBoard())
            if (i != 0)
                ++tiles;

        assertEquals(2, tiles);
    }

    @Test
    public void boardSize() {
        assertEquals(16, game.getBoard().length);
    }

    @Test
    public void isWon() {
        assertEquals(false, game.isWon());
    }
}
