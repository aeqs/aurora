package _2048;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

/**
 * Test for ever true conditions
 **/
public class GameInvariantsTest {

    private Game game;
    int preScore;

    @Before
    public void setupGame() {
        PredeterminedRNG prng = new PredeterminedRNG(0, 9, 0, 0); // white-box knowledge
        game = new Game(prng);
        preScore = game.getPlayerScore();
    }

    @Test
    public void setup() {
        int[] cmp = {
            4, 2, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0,
        };

        assertArrayEquals(cmp, game.getBoard());
        assertEquals(0, preScore);
        assertEquals(false, game.isWon());
        assertEquals(false, game.isGameOver());
    }

    @Test
    public void illegalMoves() {
        boolean legal;

        int[] cmp = {
            4, 2, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0,
        };

        // all fields stay the same
        legal = game.makeMove(MoveDirection.DOWN);
        assertArrayEquals(cmp, game.getBoard());
        assertEquals(preScore, game.getPlayerScore());
        assertEquals(false, game.isWon());
        assertEquals(false, game.isGameOver());
        assertEquals(false, legal);

        legal = game.makeMove(MoveDirection.LEFT);
        assertArrayEquals(cmp, game.getBoard());
        assertEquals(preScore, game.getPlayerScore());
        assertEquals(false, game.isWon());
        assertEquals(false, game.isGameOver());
        assertEquals(false, legal);
    }

    // test one legal move
    @Test
    public void legalMove() {
        int[] cmp = {
            4, 2, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0,
        };

        // board has to change
        boolean legal = game.makeMove(MoveDirection.UP);
        assertEquals(false, Arrays.equals(cmp, game.getBoard()));
        assertEquals(preScore, game.getPlayerScore());
        assertEquals(false, game.isWon());
        assertEquals(false, game.isGameOver());
        assertEquals(true, legal);
    }

    @Test
    public void boardChangable() {
        int[] cmp = {
            5, 2, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0,
        };

        game.getBoard()[0] = 5;
        assertArrayEquals(cmp, game.getBoard());
    }

    @Test(expected = RuntimeException.class)
    public void copyWrongLength() {
        int[] arr = new int[15];
        game.copyBoard(arr);
    }

    @Test(expected = RuntimeException.class)
    public void moveInvalidDirection() {
        game.makeMove(null);
    }

    @Test
    public void copyFields() {
        // test fields score, board
        PredeterminedRNG prng = new PredeterminedRNG(0, 9, 0, 9); // white-box knowledge
        Game test = new Game(prng);
        test.makeMove(MoveDirection.LEFT);

        assertEquals(false, test.getPlayerScore() == game.getPlayerScore());
        assertEquals(false, Arrays.equals(test.getBoard(), game.getBoard()));

        game.copyFrom(test);

        assertEquals(test.getPlayerScore(), game.getPlayerScore());
        assertArrayEquals(test.getBoard(), game.getBoard());

        // test different board instance
        game.getBoard()[0] = 5;
        assertEquals(false, Arrays.equals(test.getBoard(), game.getBoard()));
    }
}
