# CONFIGURATION

## File Format
Configuration is performed using INI files.
See [INI_files](https://wikipedia.org/wiki/INI_file) for reference.


## Configuration Parameters

### agent config
| Key                   | Value                                                    | Default Value                                  | Description                                                      |
| --------------------- | -------------------------------------------------------- | ---------------------------------------------- | ---------------------------------------------------------------- |
| `AGENT_SOURCE`        | File path                                                |                                                | Binary input file of serialized agent                            |
| `LEARNING_RATE`       | A floating-point number                                  | `0.01`                                         | Agent's learning speed                                           |
| `NETWORK`             | Refer to [Network Configuration](#network-configuration) | `0-1-4-5-8-9,1-2-5-6-9-10,2-6-10-14,3-7-11-15` | agent's network blueprint                                        |
| `SYMMETRIC_SAMPLING`  | true / false                                             | `true`                                         | If true, learning and evaluating consideres all board symmetries |

Please note that `SYMMETRIC_SAMPLING` and `NETWORK` are incompatible with the `AGENT_SOURCE` setting.
The `LEARNING_RATE` setting can be used to overwrite the learning rate of an agent imported from the `AGENT_SOURCE` file.

### run control
| Key                   | Value                                                    | Default Value                                  | Description                                                      |
| --------------------- | -------------------------------------------------------- | ---------------------------------------------- | ---------------------------------------------------------------- |
| `ITERATIONS`          | Integer value                                            | `10000`                                        | Number of games to be played                                     |
| `REDUNDANCE_RUNS`     | Integer value                                            | `5`                                            | Number of repetitions per iteration                              |

### dump files
| Key                   | Value                                                    | Default Value                                  | Description                                                      |
| --------------------- | -------------------------------------------------------- | ---------------------------------------------- | ---------------------------------------------------------------- |
| `AGENT_DUMP`          | File path                                                |                                                | Binary output file for serialized agent                          |
| `SCORE_DUMP`          | File path                                                |                                                | CSV output file for average score statistics                     |
| `WIN_RATE_DUMP`       | File path                                                |                                                | CSV output file for average win rate statistics                  |
| `AGENT_DUMP_INTERVAL` | Integer value                                            | `2000`                                         | Number of iterations between agent dumps                         |

Note: files will be newly created, overwriting.
But their directory has to be created before execution.

## Network Configuration
The network is defined by a number of tuples, each with their respective node positions.
In the configuration file, tuples are separated by a comma (`,`), and node positions within a single tuple are separated by a hyphen (`-`).

Board positions are illustrated below:
|    |    |    |    |
| -- | -- | -- | -- |
| 12 | 13 | 14 | 15 |
| 08 | 09 | 10 | 11 |
| 04 | 05 | 06 | 07 |
| 00 | 01 | 02 | 03 |


## Multiple INI Sections
Each section in the INI file corresponds to a seperate agent to be run.
The agents are run in parallel.
Entries without a section will be ignored.
